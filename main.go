package main

import (
	"bitbucket.org/ptxuvch/go-tools/ptx"
	"database/sql"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"strconv"
)

const db_dsn string = "605-db:debeljak@tcp(94.23.170.15:3306)/db_ubytovanivchorvatsku_cz"

func main() {
	r := gin.Default()

	db, err := sql.Open("mysql", db_dsn)
	defer db.Close()
	if err != nil {
		panic(err.Error())
	}
	ptx.SetDatabase(db)

	r.GET("/clients/fix/status", func(c *gin.Context) {
		ptx.UpdateClientStatus()

		c.JSON(200, gin.H{"status": true})
	})

	r.Run() // listen and serve on 0.0.0.0:8080
}
