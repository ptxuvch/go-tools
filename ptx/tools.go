package ptx

import(
    "strings"
)

func checkPanicError(e error) {
    if (e != nil) {
        panic(e.Error())
    }
}

func prefixWithZero(s string) string {
    if (len(s) == 1) {
        s = "0" + s;
    }

    return s;
}

func convertDateToDbFormat(dateString string) string {
    datePieces := strings.Split(dateString, "-");

    return datePieces[2] + "-" + prefixWithZero(datePieces[1]) + "-" + prefixWithZero(datePieces[0]);
}
